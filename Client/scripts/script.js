
function validateIPField() {
    var ip = document.getElementById('input-ip').value
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {
    return ip;
  }
  alert("Please enter a valid IPv4 Address");
  return "-1";    // error code
}   // end validateIPField()

function validatePortField() {
    // Returns "-1" if invalid port number
    var portText = document.getElementById('input-port').value
    var port = TryParseInt(portText, -1)
    if (port == -1) {   // a number was not given
        alert("Please enter a valid port number");
        return "-1";    // error code
    }   // end if{} (==-1)
    return portText;    // return the text needed to construct the 'url'
}   // end validatePortField()

function TryParseInt(str, defaultValue) {
    // Get's int from string, otherwise returns the second parameter
     var retValue = defaultValue;
     if(str !== null) {
         if(str.length > 0) {
             if (!isNaN(str)) {
                 retValue = parseInt(str);
             }
         }
     }
     return retValue;
}   // end TryParseInt()

function showDevices(devices) {
    $('.device-table tr').has('td').remove();   // first clear previous listed devices
    for (var key in devices) {
        if (devices.hasOwnProperty(key)) {
        var devData = devices[key];
        $(".device-table").append("<tr><td>" + key + "</td><td>" + devData[0] + "</td><td>" + devData[1] + "</td><td>" + devData[2] + "</td><td>" + devData[3] + "</td></tr>");
        //console.log(key + " -> " + devices[key] + " 0 = " + devices[key][0]);
        }
    }   // end for{}
}   // end showDevices()

function getMetrics(dataUrl) {
  $.ajax({
    url :  dataUrl,
    type: "GET",
    dataType: "json",
    success: function(response)  {
        //var data = JSON.stringify(response);
        //console.log(response);
        // Computer data
        $("#hostname").val(response["Computer"]["hostname"]);
        $("#os").val(response["Computer"]["os"]);
        $("#architecture").val(response["Computer"]["architecture"]);
        $("#boottime").val(response["Computer"]["boot time"]);
        $("#uptime").val(response["Computer"]["up time"]);
        $("#pythonv").val(response["Computer"]["python version"]);

        // Processor data
        $("#cpu").val(response["Processor"]["cpu name"]);
        $("#load").val(response["Processor"]["load"]);
        $("#temp").val(response["Processor"]["temperature"]);

        // Memory(RAM) data
        $("#total").val(response["Memory"]["ram"]["total"]);
        $("#available").val(response["Memory"]["ram"]["available"]);
        $("#used").val(response["Memory"]["ram"]["used"]);
        $("#usedp").val(response["Memory"]["ram"]["used percent"]);

        // Devices data
        showDevices(response["Memory"]["devices"]);

        // Network data
        $("#ip").val(response["Network"]["ip"]);
        $("#mac").val(response["Network"]["mac"]);
        $("#gateway").val(response["Network"]["gateway"]);
        $("#byreceived").val(response["Network"]["bytes received"]);
        $("#bysent").val(response["Network"]["bytes sent"]);
    },   // end success()
    error: function(jqXHR, errorThrown) {
        alert("Could not retrieve metrics from the specified device.");
        var response = "";  // empty all fields
        // Computer data
        $("#hostname").val(response);
        $("#os").val(response);
        $("#architecture").val(response);
        $("#boottime").val(response);
        $("#uptime").val(response);
        $("#pythonv").val(response);

        // Processor data
        $("#cpu").val(response);
        $("#load").val(response);
        $("#temp").val(response);

        // Memory(RAM) data
        $("#total").val(response);
        $("#available").val(response);
        $("#used").val(response);
        $("#usedp").val(response);

        // Devices data
        //$(".devices").text(response)
        $('.device-table tr').has('td').remove();

        // Network data
        $("#ip").val(response);
        $("#mac").val(response);
        $("#gateway").val(response);
        $("#byreceived").val(response);
        $("#bysent").val(response);
    }   // end fail()
  })   // end ajax{}
}    // end getMetrics()

function getData() {
    var ip = validateIPField();
    if (ip == "-1") {return;}   // invalid IP, do nothing
    var port = validatePortField();
    if (port == "-1") {return;} // invalid port, do nothing
    var dataUrl = "http://" + ip + ":" + port;
    getMetrics(dataUrl)
}   // end getData()

$(document).ready(function() {
  $("#refresh-btn").on("click", function() {
    //console.log("hello");
    getData();
  }); // END #refresh-btn callback
});