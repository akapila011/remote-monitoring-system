#!/usr/bin/env python3

import sys
import json
from pyspectator.computer import Computer
from pyspectator.processor import Processor
from pyspectator.network import NetworkInterface
from pyspectator.convert import UnitByte
import falcon
from wsgiref.simple_server import make_server


class SystemInformation(object):

    def __init__(self):
        self._computer = Computer()
        self._computer_info = None
        self._mem_info = None

        self._cpu = Processor(monitoring_latency=2)
        self._cpu_info = None

        self._net = NetworkInterface(monitoring_latency=10)
        self._net_info = None
        self.ip = self._net.ip_address

    def get_computer(self, refresh=True):
        if self._computer_info is None:
            self._computer_info = dict()
            self._computer_info["hostname"] = self._computer.hostname
            self._computer_info["os"] = self._computer.os

            self._computer_info["architecture"] = self._computer.architecture
            self._computer_info["boot time"] = self._computer.boot_time
            self._computer_info["up time"] = self._computer.uptime
            self._computer_info["python version"] = self._computer.python_version
        if refresh:
            self._computer_info["up time"] = self._computer.uptime
        return self._computer_info

    def get_processor(self, refresh=True):
        if self._cpu_info is None or refresh:
            self._cpu_info = dict()
            self._cpu_info["cpu name"] = self._cpu.name
            self._cpu_info["load"] = self._cpu.load
            self._cpu_info["temperature"] = self._cpu.temperature
        return self._cpu_info

    def get_memory(self, refresh=True):
        # NB: units in GB (remove [0] index to confirm)
        if self._mem_info is None or refresh:
            self._mem_info = dict()
            self._mem_info["ram"] = dict()
            self._mem_info["ram"]["total"] = str(UnitByte.convert(self._computer.virtual_memory.total,
                                                                  UnitByte.gigabyte))
            self._mem_info["ram"]["available"] = str(UnitByte.convert(self._computer.virtual_memory.available,
                                                                      UnitByte.gigabyte))
            self._mem_info["ram"]["used"] = str(UnitByte.convert(self._computer.virtual_memory.used, UnitByte.gigabyte))
            self._mem_info["ram"]["used percent"] = str(self._computer.virtual_memory.used_percent)
            self._mem_info["devices"] = dict()
            counter = 1
            for i in self._computer.nonvolatile_memory:
                try:
                    self._mem_info["devices"]["Memory device {0} : ".format(counter)] = [
                        UnitByte.convert(i.total, UnitByte.gigabyte),
                        UnitByte.convert(i.available, UnitByte.gigabyte),
                        UnitByte.convert(i.used, UnitByte.gigabyte),
                        i.used_percent]
                    counter += 1
                except TypeError:
                    pass
        return self._mem_info

    def get_network(self, refresh=True):
        if self._net_info is None or refresh:
            self._net_info = dict()
            self._net_info["ip"] = self._net.ip_address
            self._net_info["mac"] = self._net.hardware_address
            self._net_info["gateway"] = self._net.default_route
            self._net_info["bytes received"] = self._net.bytes_recv
            self._net_info["bytes sent"] = self._net.bytes_sent
        return self._net_info


class SystemInformationResource(object):
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.append_header("Access-Control-Allow-Origin", "*")      # to allow for cross domain requests
        data = {"Computer": system_info.get_computer(),
                "Processor": system_info.get_processor(),
                "Memory": system_info.get_memory(),
                "Network": system_info.get_network()}
        resp.body = (json.dumps(data))


def arg_is_valid_port(arg):
    try:
        port_no = int(arg)
        if port_no < 1024 or port_no < 0:
            print("Error : Port number cannot be between 0 and 1024")
            return False
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    port = 8000
    if (len(sys.argv)) == 2:     # python server.py 8000
        if not arg_is_valid_port(sys.argv[1]):  # argument is not a valid port number
            sys.exit(1)
        port = int(sys.argv[1])

    system_info = SystemInformation()
    print("System Metric Monitor started...")
    print("Monitor running on IP : " + system_info.ip)

    app = api = falcon.API()
    system_info_res = SystemInformationResource()
    api.add_route("/", system_info_res)

    with make_server("", port, app) as httpd:
        print("Listening on port {0}...".format(port))
        httpd.serve_forever()


