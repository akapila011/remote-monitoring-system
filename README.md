# Remote Monitoring System

The remote monitoring system is a simple distributed application allowing remote lookup of a device's information such as CPU load, temperature, RAM available etc.
It consists of client and server components, where the client is a simple html page making AJAX requests to the server that runs on python.

## Requirements ##
### Server requirements ###
1. Python v 3.x
2. Falcon (for REST services)
3. PySpectator (for extracting computer information)

### Client requirements ###
1. A web browser with JavaScript enabled


###How to use ##
1. Navigate to the directory where server.py is located (i.e. in the Server directory). Start the server with the following command:``` $python server.py 8000 ```
The port number is optional, and will default to 8000 if none is given.
Note: on Windows machines you make need to start the server with administrator privileges.
This ensures that your server side can access computer metrics.
2. Open the client.html page. Enter the IP address of the machine hosting the server and the port on which the server is running. Then click 'refresh' and the data will be displayed.
In case the server cannot be reached or there was a problem in retrieving the data, the browser will clear all previous data on the page.
